.PHONY : all clean


PLATFORM=LINUX_64
#PLATFORM=YLP2410


CFLAGS = -g -Wall -I./src
LDFLAGS = -lpthread -lm

ifeq ($(PLATFORM), LINUX_64)
CROSS_COMPILE:=
#CFLAGS += -m32
endif
ifeq ($(PLATFORM), YLP2410)
CROSS_COMPILE:=arm-linux-
M32=
endif

CC:= $(CROSS_COMPILE)gcc

SRC:= ./src/lrtsp_server.c \
    ./src/lrtsp_stream.c \
	./src/rtp.c \
	./src/rtsp_client.c \
	./src/rtsp_clients_manager.c \
	./src/rtsp_request_reponse.c 


OBJS := $(SRC:%.c=%.o)
DEPS_FILE :=Makefile.deps
all : $(DEPS_FILE) liblrtsp.a lrtsp_test 
lrtsp_test:lrtsp_test.c
	$(CC) $(CFLAGS) $^ -o $@ liblrtsp.a $(LDFLAGS)
$(DEPS_FILE):$(SRC)
	$(CC) -MM $(CFLAGS) $^ > $(DEPS_FILE)
liblrtsp.a :$(OBJS)
	$(AR) $(ARFLAGS) $@ $^

clean:
	rm -f liblrtsp.a lrtsp_test $(DEPS_FILE)  ./src/*.o
