#ifndef LRTSP_TYPEDEF_H
#define LRTSP_TYPEDEF_H


#ifndef return_val_if_fail
#define return_val_if_fail(p, val) if(!(p))\
			{printf("%s:%d Warning: "#p" failed.\n",\
								__func__, __LINE__); return (val);}
#endif


#ifndef return_if_fail
#define return_if_fail(p) if(!(p))\
			{printf("%s:%d Warning: "#p" failed.\n", \
									__func__, __LINE__); return;}
#endif 



#endif /*LRTSP_TYPEDEF_H*/
