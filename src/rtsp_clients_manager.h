#ifndef RTSP_CLIENTS_MANAGER_H
#define RTSP_CLIENTS_MANAGER_H

void rtsp_clients_manager_init(int max_nr);
void rtsp_clients_manager_deinit(void);

int rtsp_clients_manager_get_count(void);
struct RtspClient* rtsp_clients_manager_get(int i);
int rtsp_clients_manager_add(struct RtspClient* rtsp_client);
int rtsp_clients_manager_remove(struct RtspClient* rtsp_client);



#endif /*RTSP_CLIENTS_MANAGER_H*/

