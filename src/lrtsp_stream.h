#ifndef LRTSP_STREMH_H
#define LRTSP_STREMH_H

#include "rtsp_client.h"
#include <stdlib.h>


struct LRSDataHeader
{
    unsigned long pts;
};

struct LrtspStream
{
    char rtsp_filename[64];
    int (*pull_stream_data)(struct LrtspStream* thiz, void** stream_data, int* len, struct LRSDataHeader* header);
};

int lrtsp_stream_manager_init(void);
void lrtsp_stream_manager_deinit(void);

int lrtsp_stream_manager_add_stream(struct LrtspStream* stream);

int lrtsp_stream_add_rtsp_client(struct RtspClient* rtsp_client);
int lrtsp_stream_remove_rtsp_client(struct RtspClient* rtsp_client);


#endif /*LRTSP_STREMH_H*/
